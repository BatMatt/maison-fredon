// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  contentful: {
    spaceId: 'gkpfr9hupxui',
    token: 'pFHGPrbSDZ8rh0I8rMNU8DCnqjQuSeOWNvwlOv-RNzo',
    model: {
      about: {
        entry_id: '3jlnboVMKKDYidtF1jtmPn'
      },
      header: {
        entry_id: '5Wn1OamtwPYjxMuL1KahDy'
      },
      instagram: {
        entry_id: '5HQIsuKNCuV6oKe276e33T'
      },
      facebook: {
        entry_id: '3m9qYWgCV22th5Yn8FocRn'
      },
      tripAdvisor: {
        entry_id: '3W6VtJ4nhXFYdwvKOavRg6'
      },
      service: {
        entry_id: '2Txe5NT3IYC9IzpMntr0AU'
      },
      suite: {
        entry_id: '341lmFGZv4TeN4SIWnf312'
      }
    }
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
