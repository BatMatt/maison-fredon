export const environment = {
  production: true,
  contentful: {
    spaceId: 'gkpfr9hupxui',
    token: 'pFHGPrbSDZ8rh0I8rMNU8DCnqjQuSeOWNvwlOv-RNzo',
    model: {
      about: {
        entry_id: '3jlnboVMKKDYidtF1jtmPn'
      },
      header: {
        entry_id: '5Wn1OamtwPYjxMuL1KahDy'
      },
      instagram: {
        entry_id: '5HQIsuKNCuV6oKe276e33T'
      },
      facebook: {
        entry_id: '3m9qYWgCV22th5Yn8FocRn'
      },
      tripAdvisor: {
        entry_id: '3W6VtJ4nhXFYdwvKOavRg6'
      },
      service: {
        entry_id: '2Txe5NT3IYC9IzpMntr0AU'
      },
      suite: {
        entry_id: '341lmFGZv4TeN4SIWnf312'
      }
    }
  }
};
