import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent {

  contactForm: FormGroup;
  errorMessage: string;
  submitted: boolean = false;

  constructor(private http: HttpClient, private fb: FormBuilder) {
    this.contactForm = this.fb.group({
      fullName: ['', Validators.required],
      _replyto: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  onSubmit() {
    const formData: any = new FormData();
    formData.append('fullName', this.contactForm.get('fullName').value);
    formData.append('_replyto', this.contactForm.get('_replyto').value);
    formData.append('message', this.contactForm.get('message').value);

    this.http.post('https://formspree.io/xnqdwygp', formData).subscribe(
      (response) => this.submitted = true,
      (error) => this.errorMessage = error.error.error
    );
  }
}
