import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ReviewsCarouselComponent } from './reviews-carousel/reviews-carousel.component';



@NgModule({
  declarations: [HomeComponent, ContactFormComponent, ReviewsCarouselComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDjEBdeQI5zz-9hxCdzHm4d2isjTESxC5g',
      libraries: ['places']
    })
  ]
})
export class HomeModule { }
