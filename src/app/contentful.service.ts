import { Injectable } from '@angular/core';
import { ContentfulClientApi, createClient, Entry } from 'contentful';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class ContentfulService {
  client: ContentfulClientApi = null;
  lang: BehaviorSubject<string>;
  constructor() {
    this.client = createClient({
      space: environment.contentful.spaceId,
      accessToken: environment.contentful.token,
    });

    this.lang = new BehaviorSubject<string>("fr");
  }

  getLang() {
    return this.lang.asObservable();
  }

  setLang(lang) {
    this.lang.next(lang);
  }

  async getAbout(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "about",
        },
        { "sys.id": environment.contentful.model.about.entry_id }
      )
    );
    return res.items[0];
  }

  async getHeader(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "header",
        },
        { "sys.id": environment.contentful.model.header.entry_id }
      )
    );
    return res.items[0];
  }

  async getRooms(query?: object): Promise<Entry<any>[]> {
    return this.client
      .getEntries(
        Object.assign(
          {
            content_type: "rooms",
          },
          query
        )
      )
      .then((res) => res.items);
  }

  async getRoom(slug: string): Promise<Entry<any>> {
    return this.getRooms({ "fields.slug": slug }).then((items) => items[0]);
  }

  async getReviews(query?: object): Promise<Entry<any>[]> {
    return this.client
      .getEntries(
        Object.assign(
          {
            content_type: "reviews",
          },
          query
        )
      )
      .then((res) => res.items);
  }

  async getDiscover(query?: object): Promise<Entry<any>[]> {
    return this.client
      .getEntries(
        Object.assign(
          {
            content_type: "discover",
          },
          query
        )
      )
      .then((res) => res.items);
  }

  async getService(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "services",
        },
        { "sys.id": environment.contentful.model.service.entry_id }
      )
    );
    return res.items[0];
  }

  async getSuite(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "suites",
        },
        { "sys.id": environment.contentful.model.suite.entry_id }
      )
    );
    return res.items[0];
  }

  async getInstagram(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "instagram",
        },
        { "sys.id": environment.contentful.model.instagram.entry_id }
      )
    );
    return res.items[0];
  }

  async getFacebook(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "facebook",
        },
        { "sys.id": environment.contentful.model.facebook.entry_id }
      )
    );
    return res.items[0];
  }

  async getTripAdvisor(): Promise<Entry<any>> {
    const res = await this.client.getEntries(
      Object.assign(
        {
          content_type: "tripAdvisor",
        },
        { "sys.id": environment.contentful.model.tripAdvisor.entry_id }
      )
    );
    return res.items[0];
  }
}
