import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Entry } from 'contentful';
import { ContentfulService } from 'src/app/contentful.service';

@Component({
  selector: "app-page",
  templateUrl: "./page.component.html",
  styleUrls: ["./page.component.scss"],
})
export class PageComponent implements OnInit {
  title = "maison-fredon";
  header: Entry<any>;
  collapsed = true;
  instagram: string;
  tripAdvisor: string;
  facebook: string;

  constructor(
    private router: Router,
    private contentfulService: ContentfulService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.contentfulService.getHeader().then((header) => {
      this.header = header;
    });

    this.contentfulService.getInstagram().then((instagram) => {
      this.instagram = instagram.fields.instagramLink;
    });

    this.contentfulService.getFacebook().then((facebook) => {
      this.facebook = facebook.fields.facebookLink;
    });

    this.contentfulService.getTripAdvisor().then((tripAdvisor) => {
      this.tripAdvisor = tripAdvisor.fields.tripAdvisorLink;
    });
  }

  isHomeRoute() {
    return this.router.url === "/";
  }

  setBackgroundWhite(event) {
    const verticalOffset =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;

    if (
      verticalOffset <
      document.getElementsByClassName("js-header-navbar")[0].scrollHeight + 200
    ) {
      this.collapsed = !this.collapsed;
    }
  }

  @HostListener("window:scroll", []) onWindowScroll() {
    const verticalOffset =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;

    const headerNavbar = document.getElementsByClassName("js-header-navbar")[0];
    const btn = document.getElementsByClassName("js-header-navbar-btn")[0];

    if (this.isHomeRoute()) {
      if (
        verticalOffset >
        document.getElementsByClassName("js-header-navbar")[0].scrollHeight +
          200
      ) {
        headerNavbar.classList.add("fixed-top", "shadow-sm");
      } else {
        headerNavbar.classList.remove("fixed-top", "shadow-sm");
      }
    } else {
      if (
        verticalOffset >
        document.getElementsByClassName("js-header-navbar")[0].scrollHeight +
          200
      ) {
        headerNavbar.classList.add("fixed-top", "shadow-sm");
      } else {
        headerNavbar.classList.remove("fixed-top", "shadow-sm");
      }
    }
  }

  translateTo(lang) {
    this.translate.use(lang);
    this.contentfulService.setLang(lang);
  }
}
