import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';

import { ContentfulService } from '../contentful.service';

@Component({
  selector: "app-services",
  templateUrl: "./services.component.html",
  styleUrls: ["./services.component.scss"],
})
export class ServicesComponent implements OnInit {
  lang = "fr";

  service$: Promise<Entry<any>> = this.contentfulService.getService();
  constructor(private contentfulService: ContentfulService) {}

  ngOnInit() {
    this.contentfulService.getLang().subscribe((data) => {
      this.lang = data;
    });
  }
}
