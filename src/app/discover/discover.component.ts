import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';

import { ContentfulService } from '../contentful.service';

@Component({
  selector: "app-discover",
  templateUrl: "./discover.component.html",
  styleUrls: ["./discover.component.scss"],
})
export class DiscoverComponent implements OnInit {
  lang = "fr";

  discover$: Promise<Entry<any>[]> = this.contentfulService.getDiscover();
  constructor(private contentfulService: ContentfulService) {}

  ngOnInit() {
    this.contentfulService.getLang().subscribe((data) => {
      this.lang = data;
    });
  }
}
