import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { DiscoverRoutingModule } from './discover-routing.module';
import { DiscoverComponent } from './discover.component';

@NgModule({
  declarations: [DiscoverComponent],
  imports: [CommonModule, DiscoverRoutingModule, SharedModule],
})
export class DiscoverModule {}
