import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';

import { ContentfulService } from '../contentful.service';

@Component({
  selector: "app-rooms",
  templateUrl: "./rooms.component.html",
  styleUrls: ["./rooms.component.scss"],
})
export class RoomsComponent implements OnInit {
  lang = "fr";
  rooms$: Promise<Entry<any>[]> = this.contentfulService.getRooms();
  suite$: Promise<Entry<any>> = this.contentfulService.getSuite();
  constructor(private contentfulService: ContentfulService) {}

  ngOnInit() {
    this.contentfulService.getLang().subscribe((data) => {
      this.lang = data;
    });
  }
}
