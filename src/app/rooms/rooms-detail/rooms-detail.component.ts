import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Entry } from 'contentful';
import { ContentfulService } from 'src/app/contentful.service';

@Component({
  selector: "app-rooms-detail",
  templateUrl: "./rooms-detail.component.html",
  styleUrls: ["./rooms-detail.component.scss"],
})
export class RoomsDetailComponent implements OnInit {
  lang = "fr";

  room$: Promise<Entry<any>>;
  slug: string;
  constructor(
    private route: ActivatedRoute,
    private contentfulService: ContentfulService
  ) {}

  ngOnInit() {
    this.slug = this.route.snapshot.paramMap.get("slug");

    this.room$ = this.contentfulService.getRoom(this.slug);

    this.contentfulService.getLang().subscribe((data) => {
      this.lang = data;
    });
  }
}
