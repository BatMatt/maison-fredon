import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.scss']
})
export class ReservationFormComponent implements OnInit {

  @ViewChild('formAVP') formAVP;


  reservationForm: FormGroup;
  todayDate: Date = new Date();
  tomorrowDate: Date = new Date();
  arrivalDays: Array<any> = [];
  arrivalYearMonth: Array<any> = [];
  departureDays: Array<any> = [];
  departureYearMonth: Array<any> = [];

  adultCount: Array<any> = [{ checked: true, value: 1 }, { checked: false, value: 2 }];
  childCount: Array<any> = [{ checked: true, value: 0 }, { checked: false, value: 1 }];
  babyCount: Array<any> = [{ checked: true, value: 0 }, { checked: false, value: 1 }];


  months: Array<string> = ['Janvier', 'Février', 'Mars',
    'Avril', 'Mai', 'Juin',
    'Juillet', 'Août', 'Septembre',
    'Octobre', 'Novembre', 'Decembre'];
  constructor(private fb: FormBuilder) {
    this.tomorrowDate.setDate(this.tomorrowDate.getDate() + 1);
  }

  ngOnInit() {
    this.createForm();
    this.f.get('arrivalDate').patchValue(this.todayDate.getFullYear() + '-' + (this.todayDate.getMonth() + 1) + '-' + this.todayDate.getDate());

  }

  createForm() {
    this.reservationForm = this.fb.group({
      language: ['fr'],
      arrivalDate: [''],
      nights: [''],
      _ga: [''],
      guestCountSelector: ['ReadOnly'],
      crossSell: ['false'],
      arrivalDay: [this.todayDate.getDate(), [Validators.required]],
      arrivalMonth: [this.months[this.todayDate.getMonth()] + ' ' + this.todayDate.getFullYear(), [Validators.required]],
      departureDay: [this.tomorrowDate.getDate(), [Validators.required]],
      departureMonth: [this.months[this.todayDate.getMonth()] + ' ' + this.todayDate.getFullYear(), [Validators.required]],
      selectedAdultCount: [1, [Validators.required]],
      selectedChildCount: [0, [Validators.required]],
      selectedInfantCount: [0, [Validators.required]],
      discount: ['']
    });
  }

  initDates() {
    let y = this.todayDate.getFullYear();

    for (let i = 0; i < 23; i++) {

      const monthIndex = (this.todayDate.getMonth() + i) % 12;
      if (monthIndex === 0 && i !== 0) { y++; }

      this.arrivalYearMonth.push({
        date: new Date(1, monthIndex, y),
        month: this.months[monthIndex],
        year: y
      });

      this.departureYearMonth.push({
        date: new Date(1, monthIndex, y),
        month: this.months[monthIndex],
        year: y
      });

    }
  }

  setDepartureMonth(value) {
    const arrivalDate = new Date(
      this.f.get('arrivalMonth').value.split(' ')[1],
      this.months.indexOf(this.f.get('arrivalMonth').value.split(' ')[0]),
    );

    const departureDate = new Date(
      this.f.get('departureMonth').value.split(' ')[1],
      this.months.indexOf(this.f.get('departureMonth').value.split(' ')[0]),
    );

    let y = arrivalDate.getFullYear();

    this.departureYearMonth = [];
    for (let i = 0; i < 23; i++) {

      const monthIndex = (arrivalDate.getMonth() + i) % 12;
      if (monthIndex === 0 && i !== 0) { y++; }

      this.departureYearMonth.push({
        date: new Date(1, monthIndex, y),
        month: this.months[monthIndex],
        year: y
      });

    }
  }

  setArrivalDays(): void {
    const arrivalDate = new Date(
      this.f.get('arrivalMonth').value.split(' ')[1],
      this.months.indexOf(this.f.get('arrivalMonth').value.split(' ')[0]),
      this.f.get('arrivalDay').value
    );

    const d = arrivalDate.getDate();
    const time = new Date(arrivalDate.getTime());
    time.setMonth(arrivalDate.getMonth() + 1);
    time.setDate(0);

    const daysNumber = time.getDate() > arrivalDate.getDate() ? time.getDate() - arrivalDate.getDate() : 0;
    const result = [];
    result.push(d);

    let y = 1;
    for (let i = 1; i <= daysNumber; i++) {
      result.push(d + y);
      y++;
    }
    this.arrivalDays = result;
  }

  setDepartureDays(): void {
    const arrivalDate = new Date(
      this.f.get('arrivalMonth').value.split(' ')[1],
      this.months.indexOf(this.f.get('arrivalMonth').value.split(' ')[0]),
      this.f.get('arrivalDay').value
    );

    const d = arrivalDate.getDate() + 1;

    const time = new Date(arrivalDate.getTime());
    time.setMonth(arrivalDate.getMonth() + 1);
    time.setDate(0);

    const daysNumber = time.getDate() > arrivalDate.getDate() ? time.getDate() - arrivalDate.getDate() : 0;
    const result = [];
    result.push(d);

    let y = 1;
    for (let i = 1; i <= daysNumber; i++) {
      result.push(d + y);
      y++;
    }
    this.departureDays = result;
  }

  getDaysInMonth(monthIndex, year): Array<number> {
    const daysNumber = new Date(year, monthIndex, 0).getDate();
    const result = [];

    for (let i = 1; i <= daysNumber; i++) {
      result.push(i);
    }
    return result;
  }

  isLastDayOfMonth() {
    const today = this.todayDate;
    const lastDayOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 0);

    return today === lastDayOfMonth;
  }

  monthDiff(dateFrom, dateTo) {
    return dateTo.getMonth() - dateFrom.getMonth() +
      (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));
  }

  submitReservation() {

    if (this.f.invalid) {
      return;
    }

    const arrivalDay = this.f.get('arrivalDay').value.toString();
    const arrivalYear = this.f.get('arrivalMonth').value.split(' ')[1];


    const arrivalDate = new Date(
      arrivalYear,
      this.months.indexOf(this.f.get('arrivalMonth').value.split(' ')[0]),
      arrivalDay
    );
    const departureDate = new Date(
      this.f.get('departureMonth').value.split(' ')[1],
      this.months.indexOf(this.f.get('departureMonth').value.split(' ')[0]),
      this.f.get('departureDay').value
    );

    if (departureDate.getTime() < arrivalDate.getTime()) {
      console.log('Erreur dans la saisie des dates');
      return;
    }

    const timeDiff = Math.abs(departureDate.getTime() - arrivalDate.getTime());
    const numberOfNights = Math.ceil(timeDiff / (1000 * 3600 * 24));

    this.f.get('nights').patchValue(numberOfNights);

    this.formAVP.nativeElement.submit();
    console.log(this.f.value);
  }

  get f() {
    return this.reservationForm;
  }

  onChangeAdultCount(item) {
    this.adultCount.forEach(adult => {
      if (adult.value === item.value) {
        adult.checked = true;
      } else {
        adult.checked = false;
      }
    });

    this.f.get('selectedAdultCount').patchValue(item.value);

  }

  onChangeBabyCount(item) {
    this.babyCount.forEach(baby => {
      if (baby.value === item.value) {
        baby.checked = true;
      } else {
        baby.checked = false;
      }
    });

    this.f.get('selectedInfantCount').patchValue(item.value);
  }

  onChangeChildCount(item) {
    this.childCount.forEach(child => {
      if (child.value === item.value) {
        child.checked = true;
      } else {
        child.checked = false;
      }
    });

    this.f.get('selectedChildCount').patchValue(item.value);
  }

  onArrivalDateChange(event) {
    this.f.get('arrivalDate').patchValue(event.value.getFullYear() + '-' + (event.value.getMonth() + 1) + '-' + event.value.getDate());
    this.f.get('arrivalDay').patchValue(event.value.getDate());
    this.f.get('arrivalMonth').patchValue(this.months[event.value.getMonth()] + ' ' + event.value.getFullYear())
  }

  onDepartureDateChange(event) {
    this.f.get('departureDay').patchValue(event.value.getDate());
    this.f.get('departureMonth').patchValue(this.months[event.value.getMonth()] + ' ' + event.value.getFullYear())
  }
}
