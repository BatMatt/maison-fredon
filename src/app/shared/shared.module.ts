import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { TemplatesModule } from '../templates/templates.module';
import { MdToHtmlPipe } from './pipes/md-to-html.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [MdToHtmlPipe],
  exports: [CommonModule, TemplatesModule, MdToHtmlPipe, TranslateModule],
})
export class SharedModule {}
